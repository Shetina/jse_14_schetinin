package ru.t1.schetinin.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}