package ru.t1.schetinin.tm.api.service;

import ru.t1.schetinin.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

    Task unbindTaskFromProject(String projectId, String taskId);

}
